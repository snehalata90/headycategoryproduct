const express = require('express');
const app = express();
//const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');
app.use(bodyParser.urlencoded());


var mongoUtil = require('./module/dbConnection');

mongoUtil.connectToServer(function (err, client) {
    if (err) console.log(err);
    // console.log(client);
    const Category = require('./controller/category');
    const Product = require('./controller/product');
    app.post('/category-add', Category.addCategory);
    app.get('/category-list', Category.listCategory);
    app.post('/product-add', Product.addProduct);
    app.post('/product-list-category', Product.listProductByCategory);
    app.post('/product-update', Product.update);

    app.listen(5000, function () {
        console.log("server running on server 5000");
    });
});
