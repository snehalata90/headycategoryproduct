const express = require('express');
const app = express();
var Product = require('../module/product');

module.exports.addProduct = (req, res) => {
    var data = req.body;
    Product.add(data, (err, response) => {
        if (err) {
            res.json({ ststus: 0, message: 'something went wrong', data: err });
        } else {
            res.json({ ststus: 1, message: 'product added successfully', data: response });
        }
    })
}

module.exports.listProductByCategory = (req, res) => {
    var data = req.body;
    Product.listByCategory(data, (err, response) => {
        if (err) {
            res.json({ ststus: 0, message: 'something went wrong', data: err });
        } else {
            res.json({ ststus: 1, message: 'product added successfully', data: response });
        }
    })
}

module.exports.update = (req, res) => {
    var data = req.body;
    Product.update(data, (err, response) => {
        if (err) {
            res.json({ ststus: 0, message: 'something went wrong', data: err });
        } else {
            res.json({ ststus: 1, message: 'product updated successfully', data: response });
        }
    })
}