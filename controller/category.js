const express = require('express');
const app = express();
var Category = require('../module/category');

module.exports.addCategory = (req, res) => {
    var data = req.body;
    Category.add(data, (err, response) => {
        if (err) {
            res.json({ ststus: 0, message: 'something went wrong', data: err });
        } else {
            res.json({ ststus: 1, message: 'category added successfully', data: response });
        }
    })
}

module.exports.listCategory = (req, res) => {
    Category.list((err, response) => {
        if (err) {
            res.json({ ststus: 0, message: 'something went wrong', data: err });
        } else {
            res.json({ ststus: 1, message: 'category list', data: response });
        }
    })
}