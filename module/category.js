var mongoUtil = require('./dbConnection');

db = mongoUtil.getDb();

module.exports.add = async (data, callback) => {
    var categoryData = {
        categoryName: (data.categoryName) ? data.categoryName : '',
        subCategory: (data.subCategory) ? JSON.parse(data.subCategory) : ''
    }
    try {
        var result = await db.collection("category").insertOne(categoryData, (err, row) => {
            if (err) {
                callback(err, null);
            } else {
                var id = row.insertedId;
                callback(null, id);
            }
        });

    } catch (exception) {
        callback(exception, null)
    }
}

module.exports.list = async (callback) => {
    try {
        var result = await db.collection("category").find().toArray((err, row) => {
            if (err) {
                callback(err, null);
            } else {
                console.log(row);
                callback(null, row);
            }
        });

    } catch (exception) {
        callback(exception, null)
    }
}
