var mongoUtil = require('./dbConnection');

db = mongoUtil.getDb();

module.exports.add = async (data, callback) => {

    var category = data.productCategory;
    var productCatgory = [];
    var categoryId = JSON.parse(category);

    if (categoryId.length > 0) {
        // for (var i = 0; i < categoryId.length; i++) {
        //     var obj = {
        //         "$ref": "category",
        //         "$id": categoryId[i]['categoryId']
        //     }
        //     productCatgory.push(obj);
        // }
        for (var i = 0; i < categoryId.length; i++) {
            var obj = {
                "category": categoryId[i]['categoryId']
            }
            productCatgory.push(obj);
        }
    }
    var productData = {
        productName: (data.productName) ? data.productName : '',
        productPrice: (data.productPrice) ? data.productPrice : '',
        productVarient: (data.productVarient) ? data.productVarient : '',
        productCategory: productCatgory
    }

    try {
        var result = await db.collection("product").insertOne(productData, (err, row) => {
            if (err) {
                callback(err, null);
            } else {
                var id = row.insertedId;
                callback(null, id);
            }
        });

        // callback(null, result.insertedId)

    } catch (exception) {
        callback(exception, null)
    }
}

module.exports.listByCategory = async (data, callback) => {
    var productCategory = (data.productCategory) ? data.productCategory : '';
    try {
        var result = await db.collection("product").find({ "productCategory": { "category": productCategory } }).toArray((err, row) => {
            if (err) {
                callback(err, null);
            } else {
                // var id = row.insertedId;
                callback(null, row);
            }

        });
        // var result = await db.collection("product").find({ "productCategory": { "category._id": ObjectId(productCategory) } }).toArray((err, row) => {
        //     if (err) {
        //         callback(err, null);
        //     } else {
        //         // var id = row.insertedId;
        //         callback(null, row);
        //     }

        // });

        // callback(null, result.insertedId)

    } catch (exception) {
        callback(exception, null)
    }
}

module.exports.update = async (data, callback) => {
    var productId = (data.productId) ? data.productId : '';
    var dataList = {
        productName: (data.productName) ? data.productName : '',
        productPrice: (data.productPrice) ? data.productPrice : ''
    }

    console.log(data);
    try {
        var ObjectID = require('mongodb').ObjectID;
        var result = await db.collection("product").updateOne({ "_id": ObjectID(productId) }, { $set: dataList }, (err, row) => {
            if (err) {
                console.log(err);
                callback(err, null);
            } else {
                // var id = row.insertedId;
                callback(null, row);
            }

        });

    } catch (exception) {
        console.log(exception);
        callback(exception, null);
    }
}

